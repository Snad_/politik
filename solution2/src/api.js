const baseEndpoint = 'http://ws-old.parlament.ch/'
const API = {
    councillors: baseEndpoint+'councillors',
    councils: baseEndpoint+'councils',
    affairs: baseEndpoint+'affairs',
}

export default API
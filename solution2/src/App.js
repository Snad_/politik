import React, {useEffect, useState} from "react";
import './App.css';
import API from "./api";


function App() {
  const [councillorData, setCouncillorData] = useState(null)
  const [councillorText, setCouncillorText] = useState('')
  const [councilData, setCouncilData] = useState(null)
  const [councilText, setCouncilText] = useState('')
  const [affairData, setAffairData] = useState(null)
  const [affairText, setAffairText] = useState('')
  useEffect(()=>{

    if(!councillorData){
      fetchCouncillors()
    }

    if(!councilData){
      fetchCouncils()
    }

    if(!affairData){
      fetchAffairs()
    }

  },[])

  const fetchCouncillors = async () =>{
    const xhr = new XMLHttpRequest()
    xhr.open('GET','https://cors-anywhere.herokuapp.com/'+API.councillors,true)
    xhr.responseType = "document"
    xhr.onload = function (){
      if (xhr.readyState === 4 && xhr.status === 200){
        const response = xhr.responseXML.querySelectorAll('table')[1].querySelectorAll('tr')
        getCouncillorsData([...response])
      }
    }
    xhr.onerror = function(){
      console.log(xhr.status,xhr.statusText)
    }
    xhr.send()
  }

  const fetchCouncils = async () =>{
    const xhr = new XMLHttpRequest()
    xhr.open('GET','https://cors-anywhere.herokuapp.com/'+API.councils,true)
    xhr.responseType = "document"
    xhr.onload = function (){
      if (xhr.readyState === 4 && xhr.status === 200){
        const response = xhr.responseXML.querySelectorAll('table tbody tr')
        getCouncilsData([...response])
      }
    }
    xhr.onerror = function(){
      console.log(xhr.status,xhr.statusText)
    }
    xhr.send()
  }

  const fetchAffairs = async () =>{
    const xhr = new XMLHttpRequest()
    xhr.open('GET','https://cors-anywhere.herokuapp.com/'+API.affairs,true)
    xhr.responseType = "document"
    xhr.onload = function (){
      if (xhr.readyState === 4 && xhr.status === 200){
        const response = xhr.responseXML.querySelectorAll('table tbody tr')
        getAffairsData([...response])
      }
    }
    xhr.onerror = function(){
      console.log(xhr.status,xhr.statusText)
    }
    xhr.send()
  }

  const getText = (value) => {
    return value.replaceAll('\\n','').replaceAll(' ','')
  }

  const getCouncillorsData = (value) => {
    const result = []
    value.slice(1, value.length).forEach(e=>{
      result.push({
        id: Number(getText(e['children'][1]['outerText'])),
        number: getText(e['children'][2]['outerText']),
        firstName: getText(e['children'][3]['outerText']),
        lastName: getText(e['children'][4]['outerText']),
      })
    })
    setCouncillorData(result)
  }

  const getCouncilsData = (value) => {
    const result = []
    value.slice(1, value.length).forEach(e=>{
      result.push({
        name: getText(e['children'][1]['outerText']),
        abbreviation: getText(e['children'][2]['outerText']),
        type: getText(e['children'][3]['outerText']),
      })
    })
    setCouncilData(result)
  }

  const getAffairsData = (value) => {
    const result = []
    value.slice(1, value.length).forEach(e=>{
      result.push({
        id: Number(getText(e['children'][1]['outerText'])),
      })
    })
    setAffairData(result)
  }

  const handleSortById = ()=>{
    setCouncillorData([...councillorData.sort((a,b)=> a.id - b.id)])
  }

  const handleSortByFirstName = ()=>{
    setCouncillorData([...councillorData.sort((a,b)=> a.firstName.localeCompare(b.firstName))])
  }

  const handleSortByLastName = ()=>{
    setCouncillorData([...councillorData.sort((a,b)=> a.lastName.localeCompare(b.lastName))])
  }

  const handleSearchCouncillor = ()=>{
    return [...councillorData
        .filter(e=>
            councillorText.length < 1 ||
            (e.id.toString().toLowerCase().includes(councillorText.toLowerCase()) ||
            e.firstName.toLowerCase().includes(councillorText.toLowerCase()) ||
            e.lastName.toLowerCase().includes(councillorText.toLowerCase()))
        )
    ]
  }

  const handleCouncillorSearchChange = (e)=>{
    setCouncillorText(e.target.value)
  }

  const handleSortByName = ()=>{
    setCouncilData([...councilData.sort((a,b)=> a.name.localeCompare(b.name))])
  }

  const handleSearchCouncil = ()=>{
    return [...councilData
        .filter(e=>
            councilText.length < 1 ||
            (e.name.toString().toLowerCase().includes(councilText.toLowerCase()))
        )
    ]
  }

  const handleCouncilSearchChange = (e)=>{
    setCouncilText(e.target.value)
  }

  const handleSortByDate = ()=>{
    setAffairData([...affairData.sort((a,b)=> a.id - b.id)])
  }

  const handleSearchAffair = ()=>{
    return [...affairData
        .filter(e=>
            affairText.length < 1 ||
            (e.id.toString().toLowerCase().includes(affairText.toLowerCase()))
        )
    ]
  }

  const handleAffairSearchChange = (e)=>{
    setAffairText(e.target.value)
  }

  return (
    <div className="App">
      <div className="main">
        <div>
          <h1>Councillors</h1>
          <button onClick={handleSortById}>Sort by Id</button>
          <button onClick={handleSortByFirstName}>Sort by First Name</button>
          <button onClick={handleSortByLastName}>Sort by Last Name</button>
          <input type="text" value={councillorText} onChange={handleCouncillorSearchChange} placeholder="Search"/>
          <table>
            <thead>
            <tr>
              <th>ID</th>
              <th>Number</th>
              <th>First Name</th>
              <th>Last Name</th>
            </tr>
            </thead>
            <tbody>
            {councillorData && councillorData.length > 0 ? handleSearchCouncillor().map((e,i)=><tr key={i}>
              <td>{e.id}</td>
              <td>{e.number}</td>
              <td>{e.firstName}</td>
              <td>{e.lastName}</td>
            </tr>): <tr><td>Loading...</td></tr>}
            </tbody>
          </table>
        </div>
        <div>
          <h1>Councils</h1>
          <button onClick={handleSortByName}>Sort by Name</button>
          <input type="text" value={councilText} onChange={handleCouncilSearchChange} placeholder="Search Name"/>
          <table>
            <thead>
            <tr>
              <th>Name</th>
              <th>Abbreviation</th>
              <th>Type</th>
            </tr>
            </thead>
            <tbody>
            {councilData && councilData.length > 0 ? handleSearchCouncil().map((e,i)=><tr key={i}>
              <td>{e.name}</td>
              <td>{e.abbreviation}</td>
              <td>{e.type}</td>
            </tr>): <tr><td>Loading...</td></tr>}
            </tbody>
          </table>
        </div>
        <div>
          <h1>Affairs</h1>
          <button onClick={handleSortByDate}>Sort by Date</button>
          <input type="text" value={affairText} onChange={handleAffairSearchChange} placeholder="Search Date"/>
          <table>
            <thead>
            <tr>
              <th>Date</th>
            </tr>
            </thead>
            <tbody>
            {affairData && affairData.length > 0 ? handleSearchAffair().map((e,i)=><tr key={i}>
              <td>{e.id}</td>
            </tr>): <tr><td>Loading...</td></tr>}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default App;
